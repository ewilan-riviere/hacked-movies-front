/* eslint-disable no-unused-vars */
import Vue from 'vue'
import VueRouter from 'vue-router'
import HackedMoviesList from './views/HackedMovies/List.vue'
import HackedMoviesDetails from './views/HackedMovies/Details.vue'

Vue.use(VueRouter)

export default new VueRouter({
  name: 'Router',
  mode: 'history',
  base: process.env.BASE_URL,
  linkExactActiveClass: 'active',
  routes: [
    {
      path: '/details/:movieId',
      name: 'movie-details',
      component: HackedMoviesDetails
    }
  ]
})

// export default new Router({
//   routes: [
//     {
//       path: '/hacked-movies',
//       name: 'Hacked movies',
//       component: HackedMoviesList,
//       meta: {
//         title: 'Hacked Movies',
//         metaTags: [
//           {
//             name: 'description',
//             content: 'The home page of our example app.'
//           },
//           {
//             property: 'og:description',
//             content: 'The home page of our example app.'
//           }
//         ]
//       }
//     },
//     {
//       path: '/details/id=:id',
//       name: 'movie-details',
//       component: HackedMoviesDetails,
//       meta: {
//         title: 'Hacked Movies · Details',
//         metaTags: [
//           {
//             name: 'description',
//             content: 'The home page of our example app.'
//           },
//           {
//             property: 'og:description',
//             content: 'The home page of our example app.'
//           }
//         ]
//       }
//     }
//   ]
// })
