# Hacked Movies
Déploiement sur [Hacked Movies](http://hacked-movies.ewilan-riviere.tech/)

![](public/images/logos/hacked-movies.png)

[![Vue 2.6](https://img.shields.io/badge/Vue-2.6-green)](https://vuejs.org/)
[![NodeJS 11.15](https://img.shields.io/badge/NodeJS-11.15-green)](https://nodejs.org/en)

## Project setup
Disponible aussi avec `npm`
```bash
# Installer les dépendances
yarn install
# Lancer le serveur en développement
yarn run serve
```

### Advanced
```bash
# Compiles and minifies for production
yarn run build
# Run your tests
yarn run test
# Lints and fixes files
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
